// Get DOM Elements
const modal = document.querySelector('#my-modal');
const modalBtn = document.getElementById('modal-btn');
const closeBtn = document.querySelector('.close');
const cancelBtn = document.querySelector('.btn-cancel');
const number = document.querySelectorAll('input[type="tel"]');
const inputs = document.querySelectorAll('input');
const textareas = document.querySelectorAll('textarea');
const dropDowns = document.querySelectorAll('select');
//end of basics
const radioOne = document.querySelector('#radio-one');
const radioTwo = document.querySelector('#radio-two');
const details = document.querySelector('.vehicle-damage-details');
const mileage = document.querySelector('.mileage .textbox-container input[type="tel"]');

// Events
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
cancelBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);
radioOne.addEventListener('change', hideShow);
radioTwo.addEventListener('change', hideShow);
for (var i = 0; i < number.length; i++) {
  number[i].addEventListener('input', letOnlyNumbers, false);
}
for (var i = 0; i < inputs.length; i++) {
  inputs[i].addEventListener('focusout', checkBlanks, false);
  inputs[i].addEventListener('focusin', activateHelpers, false);
}
for (var i = 0; i < textareas.length; i++) {
  textareas[i].addEventListener('focusout', checkBlanks, false);
  textareas[i].addEventListener('focusin', activateHelpers, false);
}
for (var i = 0; i < dropDowns.length; i++) {
  dropDowns[i].addEventListener('focusout', checkBlanks, false);
}

// Generate manufacturing years
manufacturing = document.getElementById('manufacturing-year');
for (var i = new Date().getFullYear(); i >= 1950; i--) {
  var option = document.createElement("option");
  option.text = i;
  manufacturing.add(option);
}

// Generate currency options
currency = document.getElementById('currency-name');
var array = ["RON", "EUR", "DOL", "GPB"];
for (var i = 0; i < array.length; i++) {
  var option = document.createElement("option");
  option.text = array[i];
  currency.add(option);
}

// Open
function openModal() {
  modal.style.display = 'block';
}

// Close
function closeModal() {
  modal.style.display = 'none';
}

// Close If Outside Click
function outsideClick(e) {
  if (e.target == modal) {
    modal.style.display = 'none';
  }
}

// Add logo
function addLogo(logo) {
  var logo = document.getElementById(logo);
  var background = document.getElementById("logo")
  background.style.backgroundImage = "url(images/" + logo.value + ".png)";
}

// Hide/Show the accident details field
function hideShow() {
  if (document.getElementById('radio-one').checked) {
    details.classList.add("show");
    details.classList.remove("hide");
  } else {
    details.classList.remove("show");
    details.classList.add("hide");
  }
  let vehicle = document.querySelector(".vehicle-damage");
  vehicle.querySelector('.error-text').innerHTML = "";
  let selector = vehicle.querySelectorAll('label');
  selector[1].classList.remove("invalid");
  selector[2].classList.remove("invalid");
}

// All tel type fields accept only numbers
function letOnlyNumbers() {

  this.value = this.value.replace(/[^0-9]/g, '');
  //can't start with 0 by uncommenting this:
  //this.value=this.value.replace(/^0/g,'');
}
// Closer fallback for IE11
var getClosest = function (elem, selector) {

  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function (s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) { }
        return i > -1;
      };
  }

  // Get the closest matching element
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (elem.matches(selector)) return elem;
  }
  return null;

};
// Activate helpers
function activateHelpers() {
  if (!this.classList.contains("invalid")) {
    getClosest(this, ".row").querySelector('.help-text').classList.remove("hide");
    // this.closest(".row").querySelector('.help-text').classList.remove("hide");
  }
}

// Validate fields not to be empty
function checkBlanks() {
  let text = "Please complete the field";
  if (this.value == "") {
    if (getClosest(this, ".row").classList.contains('dropdown')) {
      text = "Please select option from dropdown";
    }
    else if (getClosest(this, ".row").classList.contains('number')) {
      text = "Please enter a number";
    }
    getClosest(this, ".row").querySelector('.error-text').innerHTML = text;
    this.classList.add("invalid");
    getClosest(this, ".row").querySelector('.help-text').classList.add("hide");
  }
  else {
    getClosest(this, ".row").querySelector('.error-text').innerHTML = "";
    this.classList.remove("invalid");
    getClosest(this, ".row").querySelector('.help-text').classList.add("hide");
    validateMileage();
  }
}

// Validate mileage when field is completed
function validateMileage() {

  // if(mileage.value.startsWith("0")){
  if (mileage.value.indexOf("0") == 0) {
    document.querySelector(".mileage").querySelector('.error-text').innerHTML = "Mileage number can't start with 0.";
    mileage.classList.add("invalid");
  }
  else if (!mileage.value == "") {
    document.querySelector(".mileage").querySelector('.error-text').innerHTML = "";
    mileage.classList.remove("invalid");
  }
}
//
function cheeckFields(y) {
  let text = "Please complete the field";
  if (y.value == "" && !getClosest(y, ".row").classList.contains('hide')) {
    if (getClosest(y, ".row").classList.contains('dropdown')) {
      text = "Please select option from dropdown";
    }
    else if (getClosest(y, ".row").classList.contains('number')) {
      text = "Please enter a number";
    }
    getClosest(y, ".row").querySelector('.error-text').innerHTML = text;
    y.classList.add("invalid");
    getClosest(y, ".row").querySelector('.help-text').classList.add("hide");
  }
  else {
    getClosest(y, ".row").querySelector('.error-text').innerHTML = "";
    y.classList.remove("invalid");
    getClosest(y, ".row").querySelector('.help-text').classList.add("hide");
    validateMileage();
  }
}
// Submit action
function submit() {
  for (var i = 0; i < inputs.length; i++) {
    cheeckFields(inputs[i]);
  }
  for (var i = 0; i < dropDowns.length; i++) {
    cheeckFields(dropDowns[i]);
  }
  for (var i = 0; i < textareas.length; i++) {
    cheeckFields(textareas[i]);
  }
  if (!radioOne.checked && !radioTwo.checked) {
    document.querySelector(".vehicle-damage").querySelector('.error-text').innerHTML = "Please tell us if the vehicle is damaged.";
    let selector = getClosest(radioOne, ".vehicle-damage-buttons").querySelectorAll('label');
    selector[0].classList.add("invalid");
    selector[1].classList.add("invalid");
  }
}